# --- activate a virtualenv without the need of going through a series of
#     paths
function venv { source $WORKON_HOME/$1/bin/activate; }

# --- create a virtualenv
function mkenv { virtualenv $WORKON_HOME/$1; venv $1; echo "$1" > ./.venv; }

# --- automagically load virtualenvs, based on the ".venv" file in the directory

# -- search for the ".venv" from the current directory and, if it can't be found,
#    go through the parent directories all the way to the system root
function _upwards_search {
	venv=""
	curdir=`pwd`

	while [[ `pwd` != '/' ]]; do
		if [ -f ./.venv ]; then
			venv=`cat ./.venv`
			break
		fi
		cd ..
	done

	cd $curdir
	echo $venv;
}

# -- replacement for "cd", which will check the virtualenv file
function _venv_cd { 
	if [ -d "$PWD/$*" ]; then
		if [ "$VIRTUAL_ENV." != "."  ]; then 
			deactivate
		fi
	fi
	\cd $*
	venv=$(_upwards_search)
	if [ -n "$venv" ]; then 
		venv $venv 
	fi
}

# -- replace "cd" with our magical "cd"
alias cd=_venv_cd
