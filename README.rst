Auto VirtualEnv
================

Auto-VirtualEnv is a small bash script that automatically initializes a Python
virtualenv once you get inside a directory.

Setup
------

The first step is to define where your virtualenvs will reside. You will need
to create a directory for that and then add it on your startup files (for
example, ``~/.bashrc``). To do that, simply add a line with

``export WORKON_HOME=<directory>``

where `<directory>``` is the directory for the virtualenvs.

Next step is getting the ``auto-virtualenv.sh`` file (really, all you need is
that file) and saving somewhere in your disk. Then add the following in the
end of your ``~/.bashrc``:

``source <path where auto-virtual.sh is>/auto-virtualenv.sh``

And that's it!

Autoloading VirtualEnvs
------------------------

To auto-load a virtualenv, you'll need to tell that the directory belongs to a
virtualenv. To do so, create a file named ``.venv`` inside the current directory
with the name of the virtualenv and, every time you get in this directory (or
any of its children), the virtualenv will be activated automatically.

Other tools
------------

There are two other tools included in ``auto-virtualenv.sh``:

``mkenv`` will create a virtualenv (inside your ``WORKON_HOME``) and automatically create the `.venv` in the
current directory.

``venv`` will activate any virtualenv without the need of passing the full path
(as long as the virtualenv is inside ``WORKON_HOME``).
